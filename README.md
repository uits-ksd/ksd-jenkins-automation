UA Kuali Support Scripts for Jenkins
------------------------------------

This project is a collection of resources that are used by Jenkins to do various automation
functions.

Currently this project is used by the following Jenkins instances:

  * https://ka-tools.mosaic.arizona.edu/jenkins/

