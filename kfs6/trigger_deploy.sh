#!/bin/sh
#
# Trigger Deploy
#
# This script triggers deployment of an application to the appropriate 
# environment.
#
# Usage:
#    ./trigger_deploy.sh <ENVIRONMENT>
#
#    eg.
#    ./trigger_deploy.sh dev
#
# Requirements:
#    This script requires that curl is installed on the host.
#
# The deployment process works like this:
# 1. Jenkins Job creates a file in the shared Isilon mount at 
#    /transaction/data/fs/ENV/redeploy.action where ENV is the current 
#    environment of the host (i.e. dev, tst, etc)
# 2. A cron job on each host looks every 5 minutes for the presence of a file 
#    in the appropriate location (i.e. uaz-ka-a02 checks in fs/dev/ etc).
# 3. If the check file is found:
# 4. The script kills the currently running docker container and removes it.
# 5. The script triggers the local puppet client to run, which will re-deploy 
#    the missing container.
#
# See the script docker_monitor.sh for the cron script that is run on the 
# host computers.
#
###############################################################################

# Basic logging command to put timestamps in front of things
log() {
  date +"%R $*"
}

# Read in the environment name from calling arguments
if [ "$1" != "" ]; then
  ENVNAME="$1"
else
  log "Environment parameter is required: ./trigger_deploy.sh <ENV>"
  exit 1
fi

FILENAME="redeploy.action"
TRIGGERFILE="/transactional/$ENVNAME/$FILENAME"

# Check to see if the trigger file already exists
if [ -f $TRIGGERFILE ]; then
  log "Existing trigger file found. Exiting: $TRIGGERFILE"
  # Exit with an error
  exit 1
fi

# Create the trigger file
DESCR="This file is a trigger for a script monitoring docker to remove the current container, \
and re-deploy. This file should be removed by the re-deploy script once it has run. Contact  \
the Kuali Technical Team with questions about this file."

echo $DESCR > $TRIGGERFILE

# Wait until the file disappears before completing. 
# That way we at least know the cron script on the target host ran.
COUNTER=0
while [ -f $TRIGGERFILE ]
do
  if [ "$COUNTER" -gt 40 ]; then
  	# If this hasn't been picked up after 20 minutes, error out and clean up.
    log "$TRIGGERFILE not picked up after 20 minutes."
    rm $TRIGGERFILE
    exit 1
  fi
  
  log "Waiting for $TRIGGERFILE to be picked up."
  COUNTER=$((COUNTER+1))
  sleep 30
done

# Once the trigger file has been picked up, wait to see that the site is actually responding
RESPONSECODE=000
COUNTER=0
WEBSITE="https://ka-$ENVNAME.mosaic.arizona.edu/kfs/portal.do"
while [ "$RESPONSECODE" -ne 200 ]
do
  if [ "$COUNTER" -gt 20 ]; then
    log "Invalid HTTP response code after 10 minutes: $RESPONSECODE"
    exit 1
  fi

  log "Waiting for $WEBSITE to be up: $RESPONSECODE"
  RESPONSECODE=`curl -sL -w "%{http_code}" $WEBSITE -o /dev/null`
  COUNTER=$((COUNTER+1))
  sleep 30  
done

log "$WEBSITE to is up: $RESPONSECODE"

exit 0

