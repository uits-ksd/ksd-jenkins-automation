#!/bin/bash

# UAF-978 - Create a release branch from ua-development

# Usage: release-setup-branch release release-number dev-branch master-branch
# Example: release-setup-branch UAF-408 3 ua-development ua-master

release=$1
release_number=$2
(( previous_release_number=$release_number-1 ))
dev_branch=$3
master_branch=$4
previous_release="ua-release$previous_release_number-SNAPSHOT"
current_release="ua-release$release_number"

# Create and update release branch

git fetch && git pull
git checkout -b $release $dev_branch

# update the pom files. change the release from snapshot to the new one.
yes | pom_file_replacer.sh pom.xml $previous_release $current_release

git commit -am "$release updating version to $current_release"
git merge $master_branch

git push origin $release
