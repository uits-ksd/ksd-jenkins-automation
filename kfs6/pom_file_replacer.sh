#!/bin/bash

if [ $# -ne 3 ]; then 
  echo "Replacer: a multi-file find-replace tool."   
  echo "Usage:     replacer.sh filename string_to_find string_to_replace"
  exit;
fi 

echo "Searching for instances of $2 in files named $1"
matches=$(grep -rl $2 2>/dev/null | sed -e "/$1/!d" | wc -l)
echo "Found $matches files matching this criteria"
echo "Replacing all instances of $2 with $3."
echo "Is this ok? (y/n)"

read acceptance
if [ $acceptance != "y" ];
  then exit;
fi

grep -rl $2 2>/dev/null | sed -e "/$1/!d" | xargs sed -i "s/$2/$3/g"
echo "$matches files successfully replaced."
