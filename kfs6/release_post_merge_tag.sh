#!/bin/bash

# UAF-978 - Create a release branch from ua-development

# Usage: release-post-merge-tag release release-number dev-branch master-branch
# Example: release-setup-branch UAF-408 3 ua-development ua-master

release=$1
release_number=$2
dev_branch=$3
master_branch=$4
current_release="ua-release$release_number"
dev_release="ua-release$release_number-SNAPSHOT"

git checkout $release
git pull
git merge origin/$master_branch
git checkout $master_branch

#TODO: get a list of cards that have been merged by parsing the git logs or something
git merge -m "$release merging release branch to $master_branch for release $release_number" --no-ff $release
git push origin $master_branch

# Tag the ua-master branch

git tag -a "$master_branch-release$release_number" -m "$release tagging ua-release$release_number"
git push origin --tags

# Merge changes down from release branch to development branch
git checkout $dev_branch
git merge $master_branch

# update the pom files. set the version to "SNAPSHOT" for the dev branch
yes | pom_file_replacer.sh pom.xml $current_release $dev_release

git commit -am "$release updating version to $current_release"
git push origin $dev_branch
