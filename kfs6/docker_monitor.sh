#!/bin/bash
#
# Docker Monitor
#
# This script checks for the presence of a trigger file, and depending on 
# the file found, will re-deploy docker containers.
#
# 2015-08-24: Initial version by Mark Fischer - estranged@mac.com
#
###############################################################################

LOCKFILE=/tmp/redeploy_kfs.run
DOCKER_CONTAINER_NAME=kfs

# If the lock file is present, stop ourselves and let the first one complete
if [ -f $LOCKFILE ]; then
  echo "Existing lock file found. Exiting: $LOCKFILE"
  exit 1
fi

# To sudo or not to sudo
WHO=`whoami`
if [ "$WHO" == "root" ]; then
  SUDO=""
else
  SUDO="sudo"
fi

# Figure out what host we're on
HOST=`facter hostname`
case "$HOST" in
  "workbench")
    KFSENV=dev
    ;;
  "uaz-ka-a02")
    KFSENV=dev
    ;; 
  "uaz-ka-a05")
    KFSENV=tst
    ;; 
  "uaz-ka-a07")
    KFSENV=stg
    ;; 
  *)
    echo $"Unknown Host: $HOST"
    exit 1
esac

DEPLOYFILE="/transaction/data/fs/$KFSENV/redeploy.action"

# Create our lock file to let other jobs know we're currently running
echo $! > $LOCKFILE

# Check to see if we need to re-deploy the application
echo "Checking for $DEPLOYFILE"
if [ -f $DEPLOYFILE ]; then
  echo "$DEPLOYFILE exists"
  
  # Remove the DEPLOYFILE so we don't accidentally fire again.  If something fails during this
  # process someone will have to figure out why.
  rm $DEPLOYFILE

  # Pull latest image
  $SUDO docker pull $DOCKER_CONTAINER_NAME
  
  # Stop Docker container
  $SUDO docker kill $DOCKER_CONTAINER_NAME
  
  # Remove Docker container
  $SUDO docker rm $DOCKER_CONTAINER_NAME
  
  # Trigger Puppet to re-deploy the container
  $SUDO puppet agent --test

  # Clean up all untagged (i.e. dangling) images
  $SUDO docker rmi $(docker images -f "dangling=true" -q)

fi

rm $LOCKFILE

exit 0

